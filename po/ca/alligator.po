# Translation of alligator.po to Catalan
# Copyright (C) 2020-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2020, 2022.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: alligator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-10 00:49+0000\n"
"PO-Revision-Date: 2022-07-08 12:14+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Josep M. Ferrer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "txemaq@gmail.com"

#: database.cpp:65 database.cpp:318
#, kde-format
msgid "Default"
msgstr "Predeterminat"

#: database.cpp:66 database.cpp:319
#, kde-format
msgid "Default Feed Group"
msgstr "Grup predeterminat de fonts"

#: fetcher.cpp:230
#, kde-format
msgid "Invalid XML"
msgstr "XML no vàlid"

#: fetcher.cpp:232
#, kde-format
msgid "No parser accepted the XML"
msgstr "Cap analitzador ha acceptat l'XML"

#: fetcher.cpp:234
#, kde-format
msgid "Error while parsing feed"
msgstr "Error en analitzar la font"

#: main.cpp:70
#, kde-format
msgid "Alligator"
msgstr "Alligator"

#: main.cpp:72
#, kde-format
msgid "Feed Reader"
msgstr "Lector de fonts"

#: main.cpp:74
#, kde-format
msgid "© 2020 KDE Community"
msgstr "© 2020 KDE Community"

#: main.cpp:75
#, kde-format
msgid "Tobias Fella"
msgstr "Tobias Fella"

#: main.cpp:94
#, kde-format
msgid "RSS/Atom Feed Reader"
msgstr "Lector de fonts RSS/Atom"

#: main.cpp:96
#, kde-format
msgid "Adds a new feed to database."
msgstr "Afegeix una font nova a la base de dades."

#: main.cpp:97
#, kde-format
msgid "feed URL"
msgstr "URL de la font"

#: qml/AddFeedSheet.qml:20
#, kde-format
msgid "Add Feed"
msgstr "Afegeix una font"

#: qml/AddFeedSheet.qml:25
#, kde-format
msgid "Url:"
msgstr "URL:"

#: qml/AddFeedSheet.qml:40 qml/EditFeedSheet.qml:50 qml/FeedGroupSheet.qml:49
#, kde-format
msgid "OK"
msgstr "D'acord"

#: qml/AddFeedSheet.qml:50 qml/EditFeedSheet.qml:59 qml/FeedGroupSheet.qml:60
#, kde-format
msgid "Cancel"
msgstr "Cancel·la"

#: qml/AlligatorGlobalDrawer.qml:22 qml/EntryListPage.qml:21
#: qml/EntryListPage.qml:91
#, kde-format
msgid "All Entries"
msgstr "Totes les entrades"

#: qml/AlligatorGlobalDrawer.qml:32
#, kde-format
msgid "All Feeds"
msgstr "Totes les fonts"

#: qml/AlligatorGlobalDrawer.qml:44
#, kde-format
msgid "Feed Groups"
msgstr "Grups de fonts"

#: qml/AlligatorGlobalDrawer.qml:48 qml/AlligatorGlobalDrawer.qml:51
#: qml/SettingsPage.qml:14
#, kde-format
msgid "Settings"
msgstr "Arranjament"

# skip-rule: t-apo_fim
#: qml/AlligatorGlobalDrawer.qml:54 qml/AlligatorGlobalDrawer.qml:57
#, kde-format
msgid "About"
msgstr "Quant a l'"

#: qml/AlligatorGlobalDrawer.qml:64
#, kde-format
msgid "Configure Groups"
msgstr "Configura els grups"

#: qml/EditFeedSheet.qml:19
#, kde-format
msgid "Edit Feed"
msgstr "Edita la font"

#: qml/EditFeedSheet.qml:30
#, kde-format
msgid "Display Name:"
msgstr "Nom a mostrar:"

#: qml/EditFeedSheet.qml:40
#, kde-format
msgid "Group:"
msgstr "Grup:"

#: qml/EntryListDelegate.qml:23
#, kde-format
msgctxt "by <author(s)>"
msgid "by"
msgstr "per"

#: qml/EntryListDelegate.qml:37
#, kde-format
msgid "Mark as unread"
msgstr "Marca com a no llegit"

#: qml/EntryListDelegate.qml:37
#, kde-format
msgid "Mark as read"
msgstr "Marca com a llegit"

#: qml/EntryListPage.qml:44
#, kde-format
msgid "Only Unread"
msgstr "Només no llegits"

#: qml/EntryListPage.qml:52
#, kde-format
msgid "Details"
msgstr "Detalls"

#: qml/EntryListPage.qml:63
#, kde-format
msgid "Refresh"
msgstr "Actualitza"

#: qml/EntryListPage.qml:72
#, kde-format
msgid "Error"
msgstr "Error"

#: qml/EntryListPage.qml:73 qml/EntryListPage.qml:82
#, kde-format
msgid "Error (%1): %2"
msgstr "Error (%1): %2"

#: qml/EntryListPage.qml:82
#, kde-format
msgid "No entries available"
msgstr "No hi ha cap entrada disponible"

#: qml/EntryPage.qml:43
#, kde-format
msgid "Open in Browser"
msgstr "Obre en un navegador"

#: qml/FeedDetailsPage.qml:20
#, kde-format
msgctxt "<Feed Name> - Details"
msgid "%1 - Details"
msgstr "%1 - Detalls"

#: qml/FeedDetailsPage.qml:36
#, kde-format
msgctxt "by <author(s)>"
msgid "by %1"
msgstr "per %1"

#: qml/FeedDetailsPage.qml:44
#, kde-format
msgid "Subscribed since: %1"
msgstr "Subscrit des de: %1"

#: qml/FeedDetailsPage.qml:47
#, kde-format
msgid "last updated: %1"
msgstr "darrera actualització: %1"

#: qml/FeedDetailsPage.qml:50
#, kde-format
msgid "%1 posts, %2 unread"
msgstr "%1 publicacions, %2 sense llegir"

#: qml/FeedGroupSheet.qml:24
#, kde-format
msgid "Feed Group"
msgstr "Grup de fonts"

#: qml/FeedGroupSheet.qml:32
#, kde-format
msgid "Name:"
msgstr "Nom:"

#: qml/FeedGroupSheet.qml:39
#, kde-format
msgid "Description:"
msgstr "Descripció:"

#: qml/FeedListDelegate.qml:24
#, kde-format
msgid "%1 unread entry"
msgid_plural "%1 unread entries"
msgstr[0] "%1 entrada sense llegir"
msgstr[1] "%1 entrades sense llegir"

#: qml/FeedListDelegate.qml:45
#, kde-format
msgid "Edit"
msgstr "Edita"

#: qml/FeedListPage.qml:34
#, kde-format
msgid "Refresh All Feeds"
msgstr "Actualitza totes les fonts"

#: qml/FeedListPage.qml:40
#, kde-format
msgid "Import Feeds..."
msgstr "Importa fonts..."

#: qml/FeedListPage.qml:46
#, kde-format
msgid "Export Feeds..."
msgstr "Exporta les fonts..."

#: qml/FeedListPage.qml:63
#, kde-format
msgid "Add Feed…"
msgstr "Afegeix una font…"

#: qml/FeedListPage.qml:77
#, kde-format
msgid "No feeds added yet"
msgstr "Encara no s'ha afegit cap font"

#: qml/FeedListPage.qml:105
#, kde-format
msgid "Import Feeds"
msgstr "Importa fonts"

#: qml/FeedListPage.qml:107
#, kde-format
msgid "All Files (*)"
msgstr "Tots els fitxers (*)"

#: qml/FeedListPage.qml:107
#, kde-format
msgid "XML Files (*.xml)"
msgstr "Fitxers XML (*.xml)"

#: qml/FeedListPage.qml:107
#, kde-format
msgid "OPML Files (*.opml)"
msgstr "Fitxers OPML (*.opml)"

#: qml/FeedListPage.qml:113
#, kde-format
msgid "Export Feeds"
msgstr "Exporta les fonts"

#: qml/FeedListPage.qml:115
#, kde-format
msgid "All Files"
msgstr "Tots els fitxers"

#: qml/GroupsListPage.qml:18
#, kde-format
msgid "Groups"
msgstr "Grups"

#: qml/GroupsListPage.qml:22
#, kde-format
msgid "Add Group…"
msgstr "Afegeix un grup…"

#: qml/GroupsListPage.qml:48
#, kde-format
msgid "Remove"
msgstr "Elimina"

#: qml/GroupsListPage.qml:56
#, kde-format
msgid "Set as Default"
msgstr "Estableix com a predeterminat"

#: qml/GroupsListPage.qml:65
#, kde-format
msgid "No groups created yet"
msgstr "Encara no s'ha afegit cap grup"

#: qml/GroupsListPage.qml:70
#, kde-format
msgid "Add Group"
msgstr "Afegeix un grup"

#: qml/SettingsPage.qml:20
#, kde-format
msgid "Article List"
msgstr "Llista d'articles"

#: qml/SettingsPage.qml:24
#, kde-format
msgid "Delete after:"
msgstr "Suprimeix després:"

#: qml/SettingsPage.qml:37
#, kde-format
msgid "Never"
msgstr "Mai"

#: qml/SettingsPage.qml:37
#, kde-format
msgid "Articles"
msgstr "Articles"

#: qml/SettingsPage.qml:37
#, kde-format
msgid "Days"
msgstr "Dies"

#: qml/SettingsPage.qml:37
#, kde-format
msgid "Weeks"
msgstr "Setmanes"

#: qml/SettingsPage.qml:37
#, kde-format
msgid "Months"
msgstr "Mesos"

#: qml/SettingsPage.qml:45
#, kde-format
msgid "Article"
msgstr "Article"

#: qml/SettingsPage.qml:53
#, kde-format
msgid "Font size:"
msgstr "Mida del tipus de lletra:"

#: qml/SettingsPage.qml:64
#, kde-format
msgid "Use system default"
msgstr "Usa la predeterminada del sistema"
